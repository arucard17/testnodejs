// Inicializo el Framework Express
var express = require('express');
var app = express();

var mongoose = require('mongoose');
var db = mongoose.createConnection('localhost','users');

var schema = new mongoose.Schema({id:Number,name:String,lastname:String});

User = mongoose.model('User',schema);
user = new User({id:1,name:'cristian',lastname:'acevedo'});
user.save();

// Defino el Template
app.set('view engine', 'jade');
app.set('views', __dirname + '/views');

// Assets statics
app.use(
	express.static(__dirname + '/public')
);

// Creo el Handler para /
app.get('/',function (req,res){
	res.render('index');
});

app.get('/user/:id',function (req,res){
	var name = req.params.id;
	var users = {};

	var query = User.findOne({'id':name});
	query.select('name lastname');
	query.exec(function (err,user){
		if(err) return handleError(err);
		console.log(user);
		users = user;
	});

	res.render('user',{
		locals: {
			name: users.name,
			lasname: users.lastname
		}
	});

});

app.listen(3000);
