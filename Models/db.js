var databaseUrl = 'localhost:27017/node-mongo-blog';
var collections = ['users','article'];
var db = require('mongojs').connect(databaseUrl,collections);

module.exports = db;