/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , http = require('http')
  , path = require('path')
  , db = require("./Models/db")
  , objId = db.ObjectId;


var app = express();

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(require('stylus').middleware(__dirname + '/public'));
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

  // var articleProvider= new ArticleProvider('localhost', 27017);

app.get('/', function(req, res){
  db.article.find({}, function (err,docs){
    if( err || !docs.length) console.log('No hay ningun articulo');
    res.render('index',{
      title: 'Blog',
      articles: docs
    });
  });
});

app.get('/blog/new', function(req, res) {
    res.render('blog_new', { 
        title: 'New Post'
    });
});

app.post('/blog/new', function(req, res){
  db.article.save({
    title: req.param('title'),
    body: req.param('body'),
    created_at: new Date(),
    comments: []
  }, function (err,docs){
    res.redirect('/');
  });
});

app.get('/blog/:id', function(req, res) {
  db.article.find({'_id': objId(req.params.id)}, function (err,article){
    article = article[0]
    res.render('blog_show',
    { 
      title: article.title,
      article: article
    });
  });
});

app.post('/blog/addComment', function(req, res) {
  db.article.find({'_id': objId(req.param('_id'))}, function (err,article){
    article = article[0];
    comments = article.comments;

    comments[Object.keys(comments).length]= {
      person: req.param('person'),
      comment: req.param('comment'),
      created_at: new Date()
    };

    db.article.update({'_id':article._id}, {$set: { 'comments': comments }}, function (err,updated){
      if( err || !updated ) console.log("Article not updated");
      else res.redirect('/blog/' + req.param('_id'));
    }); 
  });
});

app.listen(3000);